package de.hhu.dbs.warehouse;

import de.hhu.dbs.warehouse.db.Database;
import de.hhu.dbs.warehouse.db.DatabaseInterface;
import de.hhu.dbs.warehouse.pojo.*;

import java.time.LocalDateTime;
import java.util.List;
import javax.annotation.Nullable;
import javax.swing.ImageIcon;

@SuppressWarnings("ALL")
public abstract class Warehouse {

    /**
     * Der aktuell eingeloggte Kunde.
     */
    private Customer currentUser;

    /**
     * Die Datenbank.
     */
    protected final Database database = new Database();

    /**
     * Eine Sortierreihenfolge.
     */
    public enum SortingOrder {
        ASCENDING, DESCENDING
    }

    public Warehouse(DatabaseInterface databaseInterface) {
        this.database.connect(this.getConnectionUrl(), databaseInterface);
    }

    /**
     * Registriert einen neuen Kunden im System, falls möglich.
     *
     * <p>
     * <b>Hinweis</b>: Eine Registrierung ist nicht immer möglich.
     * </p>
     *
     * @param customer Der zu registrierende Kunde.
     */
    public abstract void register(Customer customer);

    /**
     * Loggt einen bestehenden Kunden anhand seiner E-Mail Adresse und seinem Passwort ein.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode kann zu einem beliebigen Zeitpunkt erneut aufgerufen werden. Ein erneutes Einloggen
     *                  mit anderen Benutzerdaten ist also zu jeder Zeit möglich.
     * </p>
     *
     * @param email    Die E-Mail Adresse des Kunden
     * @param password Das Passwort des Kunden
     * @return Den eingeloggten Kunden oder {@code null} falls der Kunde nicht eingeloggt werden konnte
     */
    @Nullable
    public abstract Customer login(String email, String password);

    /**
     * Aktualisiert die Profilinformationen eines Kunden.
     *
     * <p>
     * <b>Hinweis</b>: Es sollen die Profilinformationen des aktuell eingeloggten Kunden aktualisiert werden.
     * </p>
     *
     * @param update Die aktualisierten Profilinformationen
     */
    public abstract void updateProfile(Customer update);

    /**
     * Erstellt ein Newsletter-Abonnement für einen Kunden, falls möglich.
     *
     * <p>
     * <b>Hinweis</b>: Ein Kunde darf Newsletter nicht für andere Kunden abonnieren.
     * </p>
     *
     * @param newsletter Der Newsletter, welcher abonniert wird
     */
    public abstract void subscribe(Newsletter newsletter);

    /**
     * Abonniert ein Lieferabo, falls möglich.
     *
     * <p>
     * <b>Hinweis</b>: Ein Kunde darf Lieferabos nicht für andere Kunden abonnieren.
     * </p>
     *
     * @param subscription Das Lieferabo, welches abonniert wird
     */
    public abstract void subscribe(Subscription subscription);

    /**
     * Bestellt ein bestehendes Lieferabo ab, falls möglich.
     *
     * <p>
     * <b>Hinweis</b>: Ein Kunde darf Lieferabos nicht für andere Kunden abbestellen.
     * </p>
     *
     * @param subscription Das Lieferabo, welches abbestellt wird
     */
    public abstract void unsubscribe(Subscription subscription);

    /**
     * Gibt einem gewöhnlichen Kunden eine Premiummitgliedschaft, falls möglich.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param customer Der Kunde, welcher eine Premiummitgliedschaft erhalten soll
     * @param endDate  Das Datum, an dem die Premiummitgliedschaft endet
     * @param image    Der Studentenausweis, falls vorhanden
     */
    public abstract void makePremium(Customer customer, LocalDateTime endDate, ImageIcon image);

    /**
     * Erstellt einen neuen kundengebundenen Warenkorb.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von dem aktuell eingeloggten Kunden genutzt werden.
     * </p>
     *
     * @param customer Der Kunde, welcher einen neuen Warenkorb anlegen möchte
     */
    public abstract void createCart(Customer customer);

    /**
     * Entfernt einen neuen kundengebundenen Warenkorb.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur vom aktuell eingeloggten Kunden genutzt werden.
     * </p>
     *
     * @param cart Der zu entfernende Warenkorb
     */
    public abstract void removeCart(Cart cart);

    /**
     * Ermittelt alle kundengebundenen Warenkörbe.
     *
     * <p>
     * <b>Hinweis</b>: Der aktuell eingeloggte Kunde darf nur seine eigenen Warenkörbe sehen.
     * </p>
     *
     * @return Eine Liste aller zum eingeloggten Kunden gehörenden Warenkörbe oder eine leere List, falls der Kunde
     *          keine Warenkörbe besitzt.
     */
    public abstract List<Cart> getCarts();

    /**
     * Legt ein Angebot in der angegebenen Anzahl in den Warenkorb des Kunden, falls möglich.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von dem Kunden, welchem der
     *                  Warenkorb gehört, genutzt werden.
     * </p>
     *
     * @param cart     Der Warenkorb, welcher dem Kunden gehört
     * @param entry    Der Eintrag, welcher dem Warenkorb hinzugefügt werden soll
     */
    public abstract void putOffer(Cart cart, Cart.Entry entry);

    /**
     * Entfernt ein Angebot aus dem Warenkorb des Kunden, falls möglich.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von dem Kunden, welchem der
     *                  Warenkorb gehört, genutzt werden.
     * </p>
     *
     * @param cart     Der Warenkorb, welcher dem Kunden gehört
     * @param offer    Das Angebot, welches aus dem Warenkorb entfernt werden soll
     */
    public abstract void removeOffer(Cart cart, Offer offer);


    /**
     * Bestellt den Warenkorb eines Kunden, falls möglich.
     *
     * <p>
     * <b>Hinweis</b>: Kunden dürfen Warenkörbe nicht für andere Kunden bestellen.
     * </p>
     *
     * @param cart     Der Warenkorb, welcher dem Kunden gehört
     */
    public abstract void orderCart(Cart cart);

    /**
     * Erstellt einen neuen Tag, falls möglich.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param tag Der zu erstellende Tag
     */
    public abstract void createTag(Tag tag);

    /**
     * Entfernt einen bestehenden Tag, falls möglich.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden. Ein Schlagwort darf nur dann gelöscht
     *                  werden, wenn es nicht mehr in Benutzung ist.
     * </p>
     *
     * @param tag Der zu entfernende Tag
     */
    public abstract void deleteTag(Tag tag);

    /**
     * Erstellt einen neuen Artikel, falls möglich.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param article Der zu erstellende Artikel
     */
    public abstract void createArticle(Article article);

    /**
     * Entfernt einen bestehenden Artikel, falls möglich.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param article Der zu entfernende Artikel
     */
    public abstract void deleteArticle(Article article);

    /**
     * Erstellt ein neues Angebot, falls möglich.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param offer Das zu erstellende Angebot
     * @param vendor Der Anbieter
     * @param count Der Bestand
     */
    public abstract void createOffer(Offer offer, Vendor vendor, int count);

    /**
     * Entfernt ein bestehendes Angebot, falls möglich.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param offer Das zu entfernende Angebot
     */
    public abstract void deleteOffer(Offer offer);

    /**
     * Aktualisiert einen bestehenden Artikel, falls möglich.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param article Der zu aktualisierende Artikel
     */
    public abstract void updateArticle(Article article);

    /**
     * Erstellt einen neuen Newsletter, falls möglich.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param newsletter Der zu erstellende Newsletter
     */
    public abstract void createNewsletter(Newsletter newsletter);

    /**
     * Sucht nach allen bestellten Warenkörben.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @return Die Liste aller bestellten Warenkörbe oder eine leere Liste,
     * falls keine bestellten Warenkörbe existieren.
     */
    public abstract List<Cart> getOrderedCarts();

    /**
     * Fügt einem Artikel eine Empfehlung hinzu, falls möglich.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param recommender Der empfehlende Artikel
     * @param recommended Der empfohlene Artikel
     */
    public abstract void addRecommendation(Article recommender, Article recommended);

    /**
     * Entfernt eine Empfehlung eines Artikels, falls möglich.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param recommender Der empfehlende Artikel
     * @param recommended Der empfohlene Artikel
     */
    public abstract void removeRecommendation(Article recommender, Article recommended);

    /**
     * Sucht nach Artikeln anhand ihrer Namen.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode darf von allen Kunden genutzt werden.
     * </p>
     *
     * @param name Der Namen der zu suchenden Artikel
     * @return Eine Liste aller gefundenen Artikel oder eine leere Liste,
     * falls keine Artikel mit entsprechendem Namen existieren.
     */
    public abstract List<Article> searchByName(String name);

    /**
     * Ermittelt alle Angebote und sortiert sie entsprechend der angegebenen Sortierung.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode darf von allen Kunden genutzt werden.
     * </p>
     *
     * @param sortingOrder Sortierung
     * @return Eine sortierte Liste aller Angebote oder eine leere Liste, falls keine Angebote existieren.
     */
    public abstract List<Offer> getSortedOffers(SortingOrder sortingOrder);

    /**
     * Ermittelt alle Artikel die einen oder mehrere der angegebenen Tags beinhalten.
     *
     * <p>
     * <b>Hinweis</b>: Diese Methode darf von allen Kunden genutzt werden.
     * </p>
     *
     * @param tags Die zu suchenden Tags
     * @return Eine Liste aller gefundenen Tags oder eine leere Liste, falls keine Artikel gefunden wurden.
     */
    public abstract List<Article> getArticlesByTag(Tag... tags);

    public void performLogin(String email, String password) {
        this.currentUser = this.login(email, password);
    }

    public Customer getCurrentUser() {
        return this.currentUser;
    }

    public Database getDatabase() {
        return this.database;
    }

    /**
     * Löscht alle Einträge innerhalb der Datenbank.
     *
     * <p>
     * <b>Hinweis</b>: Es soll lediglich der Inhalt aller Tabellen entfernt werden.
     * </p>
     *
     */
    public abstract void deleteAll();

    public abstract String getConnectionUrl();
}
