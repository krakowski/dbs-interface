package de.hhu.dbs.warehouse.db;

import de.hhu.dbs.warehouse.db.Database;
import de.hhu.dbs.warehouse.db.table.Articles;
import de.hhu.dbs.warehouse.db.table.Carts;
import de.hhu.dbs.warehouse.db.table.Couriers;
import de.hhu.dbs.warehouse.db.table.Customers;
import de.hhu.dbs.warehouse.db.table.Employees;
import de.hhu.dbs.warehouse.db.table.Newsletters;
import de.hhu.dbs.warehouse.db.table.Offers;
import de.hhu.dbs.warehouse.db.table.PremiumCustomers;
import de.hhu.dbs.warehouse.db.table.Subscriptions;
import de.hhu.dbs.warehouse.db.table.Tags;
import de.hhu.dbs.warehouse.db.table.Vendors;

public class DatabaseInterface {

   private final Articles articles;
   private final Tags tags;
   private final Couriers couriers;
   private final Customers customers;
   private final Carts carts;
   private final Employees employees;
   private final Newsletters newsletters;
   private final Offers offers;
   private final PremiumCustomers premiumCustomers;
   private final Subscriptions subscriptions;
   private final Vendors vendors;


   public DatabaseInterface(Articles articles, Tags tags, Couriers couriers, Customers customers, Carts carts, Employees employees, Newsletters newsletters, Offers offers, PremiumCustomers premiumCustomers, Subscriptions subscriptions, Vendors vendors) {
      this.articles = articles;
      this.tags = tags;
      this.couriers = couriers;
      this.customers = customers;
      this.carts = carts;
      this.employees = employees;
      this.newsletters = newsletters;
      this.offers = offers;
      this.premiumCustomers = premiumCustomers;
      this.subscriptions = subscriptions;
      this.vendors = vendors;
   }

   public Articles getArticles() {
      return this.articles;
   }

   public Tags getTags() {
      return this.tags;
   }

   public Couriers getCouriers() {
      return this.couriers;
   }

   public Customers getCustomers() {
      return this.customers;
   }

   public Carts getCarts() {
      return this.carts;
   }

   public Employees getEmployees() {
      return this.employees;
   }

   public Newsletters getNewsletters() {
      return this.newsletters;
   }

   public Offers getOffers() {
      return this.offers;
   }

   public PremiumCustomers getPremiumCustomers() {
      return this.premiumCustomers;
   }

   public Subscriptions getSubscriptions() {
      return this.subscriptions;
   }

   public Vendors getVendors() {
      return this.vendors;
   }

   void connect(Database database) {
      this.articles.setDatabase(database);
      this.tags.setDatabase(database);
      this.couriers.setDatabase(database);
      this.customers.setDatabase(database);
      this.carts.setDatabase(database);
      this.employees.setDatabase(database);
      this.newsletters.setDatabase(database);
      this.offers.setDatabase(database);
      this.premiumCustomers.setDatabase(database);
      this.subscriptions.setDatabase(database);
      this.vendors.setDatabase(database);
   }
}
