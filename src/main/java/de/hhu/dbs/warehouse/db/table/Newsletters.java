package de.hhu.dbs.warehouse.db.table;

import de.hhu.dbs.warehouse.db.Table;
import de.hhu.dbs.warehouse.pojo.Newsletter;
import java.sql.SQLException;
import javax.annotation.Nullable;

public abstract class Newsletters extends Table<Newsletter> {

   /**
    * Fragt einen Newsletter anhand seiner eindeutigen Id ab.
    *
    * @param id Die Id des zu suchenden Newsletters
    * @return Den gefundenen Newsletter oder {@code null} falls keiner gefunden wurde
    */
   @Nullable
   public abstract Newsletter getById(long id);
}
