package de.hhu.dbs.warehouse.db;

import de.hhu.dbs.warehouse.db.Database;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public abstract class Table<T> {

   protected Connection connection;
   protected Database database;


   public void setDatabase(Database database) {
      this.database = database;
      this.connection = database.getConnection();
   }

   /**
    * Fragt alle Zeilen innerhalb dieser Tabelle ab.
    *
    * @return Eine Liste aller Einträge dieser Tabelle oder ein leere Liste, falls die
    *          Tabelle keine Einträge enthält.
    */
   public abstract List<T> getAll();

   /**
    * Fügt eine neue Zeile innerhalb dieser Tabelle ein.
    *
    * @param object Das einzufügende Objekt
    */
   public abstract void insert(T object);

   /**
    * Löscht eine Zeile innerhalb dieser Tabelle.
    *
    * @param object Das zu löschende Objekt
    */
   public abstract void delete(T object);

   /**
    * Aktualisiert eine Zeile innerhalb dieser Tabelle.
    *
    * @param object Das zu aktualisierende Objekt
    */
   public abstract void update(T object);

   /**
    * Entfernt alle Einträge dieser Tabelle.
    */
   public abstract void deleteAll();

   /**
    * Zählt die Einträge innerhalb dieser Tabelle.
    *
    * @return Die Anzahl der vorhandenen Einträge
    */
   public abstract int count();
}
