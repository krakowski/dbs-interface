package de.hhu.dbs.warehouse.db.table;

import de.hhu.dbs.warehouse.db.Table;
import de.hhu.dbs.warehouse.pojo.Cart;
import java.sql.SQLException;
import javax.annotation.Nullable;

public abstract class Carts extends Table<Cart> {

   /**
    * Fragt einen Warenkorb anhand seiner eindeutigen Id ab.
    *
    * @param id Die Id des zu suchenden Warenkorbs
    * @return Den gefundenen Warenkorb oder {@code null} falls keiner gefunden wurde
    */
   @Nullable
   public abstract Cart getById(long id);
}
