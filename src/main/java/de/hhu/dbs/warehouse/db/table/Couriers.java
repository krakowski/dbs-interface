package de.hhu.dbs.warehouse.db.table;

import de.hhu.dbs.warehouse.db.Table;
import de.hhu.dbs.warehouse.pojo.Courier;
import java.sql.SQLException;
import javax.annotation.Nullable;

public abstract class Couriers extends Table<Courier> {

   /**
    * Fragt einen Lieferdienst anhand seiner eindeutigen Bezeichnung ab.
    *
    * @param name Die Bezeichnung des zu suchenden Lieferdienstes
    * @return Den gefundenen Lieferdienst oder {@code null} falls keiner gefunden wurde
    */
   @Nullable
   public abstract Courier getByName(String name);

}
