package de.hhu.dbs.warehouse.db.table;

import de.hhu.dbs.warehouse.db.Table;
import de.hhu.dbs.warehouse.pojo.Vendor;
import java.sql.SQLException;
import javax.annotation.Nullable;

public abstract class Vendors extends Table<Vendor> {

   /**
    * Fragt einen Anbieter anhand seiner eindeutigen Bezeichnung ab.
    *
    * @param name Die Bezeichnung des zu suchenden Anbieters
    * @return Den gefundenen Anbieter oder {@code null} falls keiner gefunden wurde
    */
   @Nullable
   public abstract Vendor getByName(String name);
}
