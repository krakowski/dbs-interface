package de.hhu.dbs.warehouse.db.table;

import de.hhu.dbs.warehouse.db.Table;
import de.hhu.dbs.warehouse.pojo.Subscription;
import java.sql.SQLException;
import javax.annotation.Nullable;

public abstract class Subscriptions extends Table<Subscription> {

   /**
    * Fragt einen Warenkorb anhand seiner eindeutigen id ab.
    *
    * @param id Die Id des zu suchenden Warenkorbs
    * @return Den gefundenen Warenkorb oder {@code null} falls keiner gefunden wurde
    */
   @Nullable
   public abstract Subscription getById(long id);
}
