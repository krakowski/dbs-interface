package de.hhu.dbs.warehouse.db.table;

import de.hhu.dbs.warehouse.db.Table;
import de.hhu.dbs.warehouse.pojo.Customer;
import java.sql.SQLException;
import javax.annotation.Nullable;

public abstract class Customers extends Table<Customer> {

   /**
    * Fragt einen Kunden anhand seiner eindeutigen E-Mail Adresse ab.
    *
    * @param email Die E-Mail Adresse des zu suchenden Artikels
    * @return Den gefundenen Kunden oder {@code null} falls keiner gefunden wurde
    */
   @Nullable
   public abstract Customer getByEmail(String email);
}
