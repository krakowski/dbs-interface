package de.hhu.dbs.warehouse.db.table;

import de.hhu.dbs.warehouse.db.Table;
import de.hhu.dbs.warehouse.pojo.Employee;
import java.sql.SQLException;
import javax.annotation.Nullable;

public abstract class Employees extends Table<Employee> {

   /**
    * Fragt einen Angestellten anhand seiner eindeutigen E-Mail Adresse ab.
    *
    * @param email Die E-Mail Adresse des zu suchenden Angestellten
    * @return Den gefundenen Angestellten oder {@code null} falls keiner gefunden wurde
    */
   @Nullable
   public abstract Employee getByEmail(String email);
}
