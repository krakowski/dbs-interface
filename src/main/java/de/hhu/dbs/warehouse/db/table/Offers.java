package de.hhu.dbs.warehouse.db.table;

import de.hhu.dbs.warehouse.db.Table;
import de.hhu.dbs.warehouse.pojo.Offer;
import java.sql.SQLException;
import javax.annotation.Nullable;

public abstract class Offers extends Table<Offer> {

   /**
    * Fragt ein Angebot anhand seiner eindeutigen Id ab.
    *
    * @param id Die Id des zu suchenden Angebots
    * @return Das gefundene Angebot oder {@code null} falls keines gefunden wurde
    */
   @Nullable
   public abstract Offer getById(long id);
}
