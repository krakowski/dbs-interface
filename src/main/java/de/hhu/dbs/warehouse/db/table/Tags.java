package de.hhu.dbs.warehouse.db.table;

import de.hhu.dbs.warehouse.db.Table;
import de.hhu.dbs.warehouse.pojo.Tag;
import java.sql.SQLException;
import javax.annotation.Nullable;

public abstract class Tags extends Table<Tag> {

   /**
    * Fragt ein Schlagwort anhand seines eindeutigen Namen ab.
    *
    * @param name Der Name des zu suchenden Schlagworts
    * @return Das gefundene Schlagwort oder {@code null} falls keines gefunden wurde
    */
   @Nullable
   public abstract Tag getByName(String name);
}
