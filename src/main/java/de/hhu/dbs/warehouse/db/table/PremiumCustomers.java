package de.hhu.dbs.warehouse.db.table;

import de.hhu.dbs.warehouse.db.Table;
import de.hhu.dbs.warehouse.pojo.PremiumCustomer;
import java.sql.SQLException;
import javax.annotation.Nullable;

public abstract class PremiumCustomers extends Table<PremiumCustomer> {

   /**
    * Fragt einen Premiumkunden anhand seiner eindeutigen E-Mail Adresse ab.
    *
    * @param email Die E-Mail Adresse des zu suchenden Premiumkunden
    * @return Den gefundenen Premiumkunden oder {@code null} falls keiner gefunden wurde
    */
   @Nullable
   public abstract PremiumCustomer getByEmail(String email);

}
