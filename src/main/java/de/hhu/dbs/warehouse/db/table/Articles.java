package de.hhu.dbs.warehouse.db.table;

import de.hhu.dbs.warehouse.db.Table;
import de.hhu.dbs.warehouse.pojo.Article;
import java.sql.SQLException;
import javax.annotation.Nullable;

public abstract class Articles extends Table<Article> {

   /**
    * Fragt einen Artikel anhand seiner eindeutigen Id ab.
    *
    * @param id Die Id des zu suchenden Artikels
    * @return Den gefundenen Artikel oder {@code null} falls keiner gefunden wurde
    */
   @Nullable
   public abstract Article getById(long id);
}
