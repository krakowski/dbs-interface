package de.hhu.dbs.warehouse.db;

import de.hhu.dbs.warehouse.db.DatabaseInterface;
import de.hhu.dbs.warehouse.db.table.Articles;
import de.hhu.dbs.warehouse.db.table.Carts;
import de.hhu.dbs.warehouse.db.table.Couriers;
import de.hhu.dbs.warehouse.db.table.Customers;
import de.hhu.dbs.warehouse.db.table.Employees;
import de.hhu.dbs.warehouse.db.table.Newsletters;
import de.hhu.dbs.warehouse.db.table.Offers;
import de.hhu.dbs.warehouse.db.table.PremiumCustomers;
import de.hhu.dbs.warehouse.db.table.Subscriptions;
import de.hhu.dbs.warehouse.db.table.Tags;
import de.hhu.dbs.warehouse.db.table.Vendors;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Database {

    private Connection connection;

    private DatabaseInterface databaseInterface;

    public void connect(String connectionUrl, DatabaseInterface databaseInterface) {

        try {
            this.connection = DriverManager.getConnection(connectionUrl);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        this.databaseInterface = databaseInterface;

        databaseInterface.connect(this);
    }

    public boolean close() {

        try {
            this.connection.close();
            return true;
        } catch (SQLException e) {
            return false;
        }

    }

    public Articles getArticles() {
        return this.databaseInterface.getArticles();
    }

    public Couriers getCouriers() {
        return this.databaseInterface.getCouriers();
    }

    public Tags getTags() {
        return this.databaseInterface.getTags();
    }

    public Customers getCustomers() {
        return this.databaseInterface.getCustomers();
    }

    public Carts getCarts() {
        return this.databaseInterface.getCarts();
    }

    public Employees getEmployees() {
        return this.databaseInterface.getEmployees();
    }

    public Newsletters getNewsletters() {
        return this.databaseInterface.getNewsletters();
    }

    public Offers getOffers() {
        return this.databaseInterface.getOffers();
    }

    public PremiumCustomers getPremiumCustomers() {
        return this.databaseInterface.getPremiumCustomers();
    }

    public Subscriptions getSubscriptions() {
        return this.databaseInterface.getSubscriptions();
    }

    public Vendors getVendors() {
        return this.databaseInterface.getVendors();
    }

    /**
     * Gibt eine Liste aller Tabellen der Datenbank zurück.
     *
     * @return Die Liste aller Tabellen der Datenbank.
     */
    public List<Table> getTables() {
        List<Table> result = new ArrayList<>();

        result.addAll(Arrays.asList(
                getArticles(),
                getCouriers(),
                getTags(),
                getCustomers(),
                getCarts(),
                getEmployees(),
                getNewsletters(),
                getOffers(),
                getPremiumCustomers(),
                getSubscriptions(),
                getVendors()
        ));

        return result;
    }

    Connection getConnection() {
        return this.connection;
    }
}
