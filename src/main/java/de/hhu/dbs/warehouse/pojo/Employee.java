package de.hhu.dbs.warehouse.pojo;

import de.hhu.dbs.warehouse.pojo.Address;
import de.hhu.dbs.warehouse.pojo.Customer;
import java.math.BigDecimal;

public class Employee extends Customer {

    /**
     * Die Jobbezeichnung dieses Angestellten.
     */
    private String job;

    /**
     * Das Gehalt dieses Angestellten.
     */
    private BigDecimal salary;

    public Employee(String email, String password, String firstname, String lastname, Address address, String job, BigDecimal salary) {
        super(email, password, firstname, lastname, address);
        this.job = job;
        this.salary = salary;
    }

    public String getJob() {
        return this.job;
    }

    public BigDecimal getSalary() {
        return this.salary;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Employee employee = (Employee) o;

        if (!job.equals(employee.job)) return false;
        return salary.equals(employee.salary);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + job.hashCode();
        result = 31 * result + salary.hashCode();
        return result;
    }
}
