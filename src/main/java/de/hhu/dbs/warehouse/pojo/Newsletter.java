package de.hhu.dbs.warehouse.pojo;

import java.util.ArrayList;

public class Newsletter {

    /**
     * Die E-Mail Adresse des Angestellten, welcher diesen Newsletter erstellt hat.
     */
    private String employee;

    /**
     * Der Inhalt dieses Newsletters.
     */
    private String text;

    /**
     * Der Betreff dieses Newsletters.
     */
    private String subject;

    /**
     * Die Ids aller in diesem Newsletter enthaltenen Artikel.
     */
    private ArrayList<Long> articleIds;

    public Newsletter(String employee, String text, String subject, ArrayList<Long> articleIds) {
        this.employee = employee;
        this.text = text;
        this.subject = subject;
        this.articleIds = articleIds;
    }

    public String getEmployee() {
        return this.employee;
    }

    public String getText() {
        return this.text;
    }

    public String getSubject() {
        return this.subject;
    }

    public ArrayList getArticleIds() {
        return this.articleIds;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setArticleIds(ArrayList<Long> articleIds) {
        this.articleIds = articleIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Newsletter that = (Newsletter) o;

        if (!employee.equals(that.employee)) return false;
        if (!text.equals(that.text)) return false;
        return subject.equals(that.subject);
    }

    @Override
    public int hashCode() {
        int result = employee.hashCode();
        result = 31 * result + text.hashCode();
        result = 31 * result + subject.hashCode();
        return result;
    }
}
