package de.hhu.dbs.warehouse.pojo;

import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.List;

public class Cart {

    /**
     * Die Id dieses Warenkorbs.
     */
    private long id;

    /**
     * Das Bestelldatum dieses Warenkorbs, falls vorhanden.
     */
    @Nullable
    private LocalDateTime orderDate;

    /**
     * Die Bezeichnung des Lieferdienstes, welcher diesen Warenkorb zustellt, falls vorhanden.
     */
    @Nullable
    private String courier;

    /**
     * Der Bestellstatus dieses Warenkorbs.
     */
    private String status;

    /**
     * Die E-Mail Adresse des zu diesem Warenkorb gehörenden Kunden.
     */
    private String customer;

    /**
     * Alle Einträge innerhalb dieses Warenkorbs.
     */
    private List<Entry> entries;

    public Cart(long id, @Nullable LocalDateTime orderDate, @Nullable String courier, String status, String customer, List<Entry> entries) {
        this.id = id;
        this.orderDate = orderDate;
        this.courier = courier;
        this.status = status;
        this.customer = customer;
        this.entries = entries;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Nullable
    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(@Nullable LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    @Nullable
    public String getCourier() {
        return courier;
    }

    public void setCourier(@Nullable String courier) {
        this.courier = courier;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cart cart = (Cart) o;

        if (id != cart.id) return false;
        if (orderDate != null ? !orderDate.equals(cart.orderDate) : cart.orderDate != null) return false;
        if (courier != null ? !courier.equals(cart.courier) : cart.courier != null) return false;
        if (!status.equals(cart.status)) return false;
        return customer.equals(cart.customer);
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (orderDate != null ? orderDate.hashCode() : 0);
        result = 31 * result + (courier != null ? courier.hashCode() : 0);
        result = 31 * result + status.hashCode();
        result = 31 * result + customer.hashCode();
        return result;
    }

    public class Entry {

        /**
         * Die Id des zu diesem Eintrag gehörenden Angebots.
         */
        private long offer;

        /**
         * Die Id des zu diesem Eintrag gehörenden Anbieters.
         */
        private long vendor;

        /**
         * Die Anzahl, in der dieser Eintrag im Warenkorb gelistet ist.
         */
        private int count;

        public Entry(long offer, long vendor, int count) {
            this.offer = offer;
            this.vendor = vendor;
            this.count = count;
        }

        public long getOffer() {
            return offer;
        }

        public void setOffer(long offer) {
            this.offer = offer;
        }

        public long getVendor() {
            return vendor;
        }

        public void setVendor(long vendor) {
            this.vendor = vendor;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Entry entry = (Entry) o;

            if (offer != entry.offer) return false;
            if (vendor != entry.vendor) return false;
            return count == entry.count;
        }

        @Override
        public int hashCode() {
            int result = (int) (offer ^ (offer >>> 32));
            result = 31 * result + (int) (vendor ^ (vendor >>> 32));
            result = 31 * result + count;
            return result;
        }
    }
}
