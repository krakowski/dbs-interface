package de.hhu.dbs.warehouse.pojo;

import de.hhu.dbs.warehouse.pojo.Address;
import de.hhu.dbs.warehouse.pojo.Customer;
import java.time.LocalDateTime;
import java.util.List;
import javax.annotation.Nullable;
import javax.swing.ImageIcon;

public class PremiumCustomer extends Customer {

    /**
     * Das Ablaufdatum der Premiummitgliedschaft.
     */
    private LocalDateTime expirationDate;

    /**
     * Der Studentenausweis, falls vorhanden.
     */
    @Nullable
    private ImageIcon picture;

    public PremiumCustomer(String email, String password, String firstname, String lastname, Address address, LocalDateTime expirationDate, @Nullable ImageIcon picture) {
        super(email, password, firstname, lastname, address);
        this.expirationDate = expirationDate;
        this.picture = picture;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Nullable
    public ImageIcon getPicture() {
        return picture;
    }

    public void setPicture(@Nullable ImageIcon picture) {
        this.picture = picture;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PremiumCustomer that = (PremiumCustomer) o;

        return expirationDate.equals(that.expirationDate);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + expirationDate.hashCode();
        return result;
    }
}
