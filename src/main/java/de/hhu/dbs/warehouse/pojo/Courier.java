package de.hhu.dbs.warehouse.pojo;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Courier {

    /**
     * Die Bezeichnung des Lieferdienstes.
     */
    private String name;

    /**
     * Der Tarif des Lieferdienstes.
     */
    private BigDecimal rate;

    public Courier(String name, BigDecimal rate) {
        this.name = name;
        this.rate = rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Courier courier = (Courier) o;

        if (!name.equals(courier.name)) return false;
        return rate.equals(courier.rate);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + rate.hashCode();
        return result;
    }
}
