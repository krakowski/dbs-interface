package de.hhu.dbs.warehouse.pojo;

import java.util.ArrayList;

public class Vendor {

    /**
     * Der Name des Anbieters.
     */
    private String name;

    /**
     *  Die Bestände des Anbieters.
     */
    private ArrayList<Stock> stocks;

    public Vendor(String name, ArrayList<Stock> stocks) {
        this.name = name;
        this.stocks = stocks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vendor vendor = (Vendor) o;

        return name.equals(vendor.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public class Stock {

        /**
         * Die Id des zu diesem Bestand gehörenden Angebots.
         */
        private long offer;

        /**
         * Die Anzahl, in der das Angebot in diesem Bestand vorhanden ist.
         */
        private int count;

        public Stock(long offer, int count) {
            this.offer = offer;
            this.count = count;
        }

        public long getOffer() {

            return offer;
        }

        public void setOffer(long offer) {
            this.offer = offer;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Stock stock = (Stock) o;

            if (offer != stock.offer) return false;
            return count == stock.count;
        }

        @Override
        public int hashCode() {
            int result = (int) (offer ^ (offer >>> 32));
            result = 31 * result + count;
            return result;
        }
    }
}
