package de.hhu.dbs.warehouse.pojo;


public class Address {

    /**
     * Der Straßenname.
     */
    private String street;

    /**
     * Die Hausnummer.
     */
    private String houseNumber;

    /**
     * Die Postleitzahl.
     */
    private int postalCode;

    /**
     * Der Ortsname.
     */
    private String city;

    public Address(String street, String houseNumber, int postalCode, String city) {
        this.street = street;
        this.houseNumber = houseNumber;
        this.postalCode = postalCode;
        this.city = city;
    }

    public String getStreet() {
        return this.street;
    }

    public String getHouseNumber() {
        return this.houseNumber;
    }

    public int getPostalCode() {
        return this.postalCode;
    }

    public String getCity() {
        return this.city;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address = (Address) o;

        if (postalCode != address.postalCode) return false;
        if (!street.equals(address.street)) return false;
        if (!houseNumber.equals(address.houseNumber)) return false;
        return city.equals(address.city);
    }

    @Override
    public int hashCode() {
        int result = street.hashCode();
        result = 31 * result + houseNumber.hashCode();
        result = 31 * result + postalCode;
        result = 31 * result + city.hashCode();
        return result;
    }
}
