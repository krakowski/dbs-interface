package de.hhu.dbs.warehouse.pojo;


public class Tag {

    /**
     * Das Schlagwort.
     */
    private String word;

    public Tag(String word) {
        this.word = word;
    }

    public String getWord() {
        return this.word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tag tag = (Tag) o;

        return word.equals(tag.word);
    }

    @Override
    public int hashCode() {
        return word.hashCode();
    }
}
