package de.hhu.dbs.warehouse.pojo;

import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.List;

public class Subscription extends Cart{

    /**
     * Das Startdatum des Lieferabos.
     */
    private LocalDateTime begin;

    /**
     * Das Enddatum des Lieferabos.
     */
    private LocalDateTime end;

    /**
     * Das Intervall des Lieferabos in ganzen Tagen.
     */
    private int interval;

    public Subscription(long id, @Nullable LocalDateTime orderDate, @Nullable String courier, String status, String customer, List<Entry> entries, LocalDateTime begin, LocalDateTime end, int interval) {
        super(id, orderDate, courier, status, customer, entries);
        this.begin = begin;
        this.end = end;
        this.interval = interval;
    }

    public LocalDateTime getBegin() {
        return this.begin;
    }

    public LocalDateTime getEnd() {
        return this.end;
    }

    public int getInterval() {
        return this.interval;
    }

    public void setBegin(LocalDateTime begin) {
        this.begin = begin;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Subscription that = (Subscription) o;

        if (interval != that.interval) return false;
        if (!begin.equals(that.begin)) return false;
        return end.equals(that.end);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + begin.hashCode();
        result = 31 * result + end.hashCode();
        result = 31 * result + interval;
        return result;
    }
}
