package de.hhu.dbs.warehouse.pojo;

import de.hhu.dbs.warehouse.pojo.Address;

public class Customer {

    /**
     * Die E-Mail Adresse des Kunden.
     */
    private String email;

    /**
     * Das Passwort des Kunden.
     */
    private String password;

    /**
     * Der Vorname des Kunden.
     */
    private String firstname;

    /**
     * Der Nachname des Kunden.
     */
    private String lastname;

    /**
     * Die Adresse des Kunden.
     */
    private Address address;

    public Customer(String email, String password, String firstname, String lastname, Address address) {
        this.email = email;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
    }

    public String getEmail() {
        return this.email;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public Address getAddress() {
        return this.address;
    }

    public String getPassword() {
        return this.password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        if (!email.equals(customer.email)) return false;
        if (!password.equals(customer.password)) return false;
        if (!firstname.equals(customer.firstname)) return false;
        if (!lastname.equals(customer.lastname)) return false;
        return address.equals(customer.address);
    }

    @Override
    public int hashCode() {
        int result = email.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + firstname.hashCode();
        result = 31 * result + lastname.hashCode();
        result = 31 * result + address.hashCode();
        return result;
    }
}
