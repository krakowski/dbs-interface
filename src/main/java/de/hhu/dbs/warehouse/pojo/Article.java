package de.hhu.dbs.warehouse.pojo;

import java.util.ArrayList;
import javax.annotation.Nullable;
import javax.swing.ImageIcon;

public class Article {

    /**
     * Die Id dieses Artikels.
     */
    private long id;

    /**
     * Die Bezeichnung dieses Artikels.
     */
    private String name;

    /**
     * Die Beschreibung dieses Artikels.
     */
    private String description;

    /**
     * Die Ids aller zu diesem Artikel gehörenden Schlagwörter.
     */
    private ArrayList<Long> tags;

    /**
     * Die Ids aller von diesem Artikel empfohlenen Artikel.
     */
    private ArrayList<Long> recommendations;

    /**
     * Das zu diesem Artikel gehörende Bild, falls vorhanden.
     */
    @Nullable
    private ImageIcon picture;

    public Article(long id, String name, String description, ArrayList<Long> tags, ArrayList<Long> recommendations, @Nullable ImageIcon picture) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.tags = tags;
        this.recommendations = recommendations;
        this.picture = picture;
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public ArrayList getTags() {
        return this.tags;
    }

    public ArrayList getRecommendations() {
        return this.recommendations;
    }

    public ImageIcon getPicture() {
        return this.picture;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTags(ArrayList<Long> tags) {
        this.tags = tags;
    }

    public void setRecommendations(ArrayList<Long> recommendations) {
        this.recommendations = recommendations;
    }

    public void setPicture(@Nullable ImageIcon picture) {
        this.picture = picture;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Article article = (Article) o;

        if (id != article.id) return false;
        if (!name.equals(article.name)) return false;
        return description.equals(article.description);
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + name.hashCode();
        result = 31 * result + description.hashCode();
        return result;
    }
}
