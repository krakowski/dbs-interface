package de.hhu.dbs.warehouse.pojo;

import java.math.BigDecimal;

public class Offer {

    /**
     * Die Id dieses Angebots
     */
    private long id;

    /**
     * Die Id des zu diesem Angebot gehörenden Artikels.
     */
    private long article;

    /**
     * Der zu diesem Angebot gehörende Preis.
     */
    private BigDecimal price;

    public Offer(long id, long article, BigDecimal price) {
        this.id = id;
        this.article = article;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getArticle() {
        return article;
    }

    public void setArticle(long article) {
        this.article = article;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Offer offer = (Offer) o;

        if (id != offer.id) return false;
        if (article != offer.article) return false;
        return price.equals(offer.price);
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (article ^ (article >>> 32));
        result = 31 * result + price.hashCode();
        return result;
    }
}
